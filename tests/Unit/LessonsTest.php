<?php

namespace Tests\Unit;

use App\Lesson;
use Tests\Helpers\APITester;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LessonsTest extends APITester
{
    use \Tests\Helpers\Factory;

    /** @test */
    public function it_fetches_lessons()
    {
        $this->make('App\Lesson');

        $response = $this->getJson('api/v1/lessons');

        $response->assertStatus(200);
    }

    /** @test */
    public function it_fetches_a_single_lesson()
    {
        $this->make('App\Lesson');

        $response = $this->getJson('api/v1/lessons/1');

        $response->assertStatus(200);

        $this->assertJsonFragments($response, 'body', 'active');
    }

    /** @test */
    public function it_returns_404_if_a_lesson_is_not_found()
    {
        $response = $this->getJson('api/v1/lessons/x');

        $response->assertStatus(404);
        $response->assertJsonFragment(['error']);
    }

    /** @test */
    public function it_creates_a_new_lesson_given_valid_parameters()
    {
        $response = $this->json('POST', 'api/v1/lessons', $this->getStub());

        $response->assertStatus(201);
    }

    /** @test */
    public function it_returns_422_if_a_new_lesson_request_fails_validation()
    {
        $response = $this->json('POST', 'api/v1/lessons');

        $response->assertStatus(422);
    }

    protected function getStub()
    {
        return [
            'title' => $this->fake->sentence,
            'body' => $this->fake->paragraph
        ];
    }
}
