<?php

namespace Tests\Helpers;

use Artisan;
use Tests\TestCase;
use Faker\Factory as Faker;

abstract class APITester extends TestCase
{
    protected $fake;

    public function __construct()
    {
        $this->fake = Faker::create();
    }

    protected function setUp()
    {
        parent::setUp();

        Artisan::call('migrate');
    }

    protected function assertJsonFragments(...$attributes)
    {
        $response = array_shift($attributes);

        foreach ($attributes as $attribute) {
            $response->assertJsonFragment((array) $attribute);
        }
    }
}
